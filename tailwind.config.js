module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: {
        'max-content': 'max-content',
      },
      width: {
        'max-content': 'max-content',
      },
      colors: {
        primary: {
          1000: '#25476A',
        }
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
