import axios from 'axios'

const baseURL = 'http://localhost:8000/api/customers'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class CustomersAPI {
  static getAll = (token: string) => axios.get(baseURL, configWithBearerToken(token))

  static getAllWithOnlyName = (token: string) => axios.get(`${baseURL}/onlyname`, configWithBearerToken(token))

  static get = (id: number, token: string) => axios.get(`${baseURL}/${id}`, configWithBearerToken(token))

  static getWithOnlyName = (id: number, token: string) => axios.get(`${baseURL}/${id}/onlyname`, configWithBearerToken(token))

  static create = (userIdIn: number, codenameIn: string, token: string) => axios.post(baseURL, {
    user_id: userIdIn,
    codename: codenameIn,
  },
  configWithBearerToken(token))

  static update = (idIn: number, userIdIn: number, codenameIn: string, token: string) => axios.put(`${baseURL}/${idIn}`, {
    id: idIn,
    user_id: userIdIn,
    codename: codenameIn,
  },
  configWithBearerToken(token))

  static delete = (idIn: number, token: string) => axios.delete(`${baseURL}/${idIn}`, configWithBearerToken(token))
}
