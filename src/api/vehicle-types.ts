import axios from 'axios'

const baseURL = 'http://localhost:8000/api/vehicle-types'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class VehicleTypesAPI {
  static getAll = (token: string) => axios.get(baseURL, configWithBearerToken(token))

  static get = (id: number, token: string) => axios.get(`${baseURL}/${id}`, configWithBearerToken(token))

  static create = ( nameIn: string, descriptionIn: string, token: string) => axios.post(baseURL, {
    name: nameIn,
    description: descriptionIn
  },
  configWithBearerToken(token))

  static update = (idIn: string, nameIn: string, descriptionIn: string, token: string) => axios.put(`${baseURL}/${idIn}`, {
    name: nameIn,
    description: descriptionIn
  },
  configWithBearerToken(token))

  static delete = (idIn: number, token: string) => axios.delete(`${baseURL}/${idIn}`, configWithBearerToken(token))
}
