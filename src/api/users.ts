import axios from 'axios'

const baseURL = 'http://localhost:8000/api/users'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class UsersAPI {
  static getAll = (token: string) => axios.get(baseURL, configWithBearerToken(token))

  static get = (id: number, token: string) => axios.get(`${baseURL}/${id}`, configWithBearerToken(token))

  // static create = (usernameIn: string, nameIn: string, emailIn: string, passwordIn: string, avatarIn: string, roleIn: number, statusIn: string, token: string) => axios.post(baseURL, {
  //   codename: codenameIn,
  //   name: nameIn,
  //   phone: phoneIn,
  //   email: emailIn,
  //   address: addressIn,
  // },
  // configWithBearerToken(token))

  static update = (idIn: string, usernameIn: string, nameIn: string, emailIn: string,
    avatarIn: string, phoneIn: string, addressIn: string, roleIn: number, activeIn: number,
    accessToken: string) => axios.put(`${baseURL}/${idIn}`, {
      id: idIn,
      username: usernameIn,
      name: nameIn,
      email: emailIn,
      avatar: avatarIn,
      phone: phoneIn,
      address: addressIn,
      role: roleIn,
      active: activeIn,
    },
      configWithBearerToken(accessToken))

  static delete = (idIn: number, token: string) => axios.delete(`${baseURL}/${idIn}`, configWithBearerToken(token))
}
