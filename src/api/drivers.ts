import axios from 'axios'

const baseURL = 'http://localhost:8000/api/drivers'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class DriversAPI {
  static getAll = (token: string) => axios.get(baseURL, configWithBearerToken(token))

  static getAllWithOnlyName = (token: string) => axios.get(`${baseURL}/onlyname`, configWithBearerToken(token))

  static get = (id: number, token: string) => axios.get(`${baseURL}/${id}`, configWithBearerToken(token))

  static getWithOnlyName = (id: number, token: string) => axios.get(`${baseURL}/${id}/onlyname`, configWithBearerToken(token))

  static create = (userIdIn: number, driverGroupIdIn: number, vehicleTypeId: number, codenameIn: string,
    citizenIdIn: string, licenseIdIn: string, licensePlateIn: string, token: string) => axios.post(baseURL, {
      user_id: userIdIn,
      driver_group_id: driverGroupIdIn,
      vehicle_type_id: vehicleTypeId,
      codename: codenameIn,
      citizen_id: citizenIdIn,
      license_id: licenseIdIn,
      license_plate: licensePlateIn
  },
  configWithBearerToken(token))

  static update = (idIn: number, userIdIn: number, driverGroupIdIn: number, vehicleTypeId: number, codenameIn: string,
    citizenIdIn: string, licenseIdIn: string, licensePlateIn: string, token: string) => axios.put(`${baseURL}/${idIn}`, {
      user_id: userIdIn,
      driver_group_id: driverGroupIdIn,
      vehicle_type_id: vehicleTypeId,
      codename: codenameIn,
      citizen_id: citizenIdIn,
      license_id: licenseIdIn,
      license_plate: licensePlateIn
  },
  configWithBearerToken(token))

  static delete = (idIn: number, token: string) => axios.delete(`${baseURL}/${idIn}`, configWithBearerToken(token))
}
