import axios from 'axios'

const baseURL = 'http://localhost:8000/api/categories'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class CategoriesAPI {
  static getAll = (token: string) => axios.get(baseURL, configWithBearerToken(token))

  // To do: get by filter != 0

  static get = (id: number, token: string) => axios.get(`${baseURL}/${id}`, configWithBearerToken(token))

  static create = (nameIn: string, descriptionIn: string, parentIdIn: number, activeIn: string, token: string) => axios.post(baseURL, {
    name: nameIn,
    description: descriptionIn,
    parent_id: parentIdIn,
    active: activeIn,
  },
  configWithBearerToken(token))

  static update = (idIn: number, nameIn: string, descriptionIn: string, parentIdIn: number, activeIn: string, token: string) => axios.put(`${baseURL}/${idIn}`, {
    name: nameIn,
    description: descriptionIn,
    parent_id: parentIdIn,
    active: activeIn,
  },
  configWithBearerToken(token))

  static delete = (idIn: number, token: string) => axios.delete(`${baseURL}/${idIn}`, configWithBearerToken(token))
}
