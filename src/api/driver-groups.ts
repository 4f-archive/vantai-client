import axios from 'axios'

const baseURL = 'http://localhost:8000/api/driver-groups'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class DriverGroupsAPI {
  static getAll = (token: string) => axios.get(baseURL, configWithBearerToken(token))

  static get = (id: number, token: string) => axios.get(`${baseURL}/${id}`, configWithBearerToken(token))

  static create = (leaderIdIn: number, codenameIn: string, nameIn: string, token: string) => axios.post(baseURL, {
    leader_id: leaderIdIn,
    codename: codenameIn,
    name: nameIn
  },
  configWithBearerToken(token))

  static update = (idIn: number, leaderIdIn: number, codenameIn: string, nameIn: string, token: string) => axios.put(`${baseURL}/${idIn}`, {
    id: idIn,
    leader_id: leaderIdIn,
    codename: codenameIn,
    name: nameIn
  },
  configWithBearerToken(token))

  static delete = (idIn: number, token: string) => axios.delete(`${baseURL}/${idIn}`, configWithBearerToken(token))
}
