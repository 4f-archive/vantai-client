import axios from 'axios'

const baseURL = 'http://localhost:8000/api/orders'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class OrdersAPI {
  static getAll = (token: string) => axios.get(baseURL, configWithBearerToken(token))

  static get = (id: number, token: string) => axios.get(`${baseURL}/${id}`, configWithBearerToken(token))

  static getWaitingOrdersCount = (driverId: number, token: string) => axios.get(`${baseURL}/${driverId}/waitingorderscount`, configWithBearerToken(token))

  static getOrdersData = (token: string) => axios.get(`${baseURL}/data`, configWithBearerToken(token))

  static create = (categoryIdIn: number, customerIdIn: number, driverIdIn: number, pickupAddressIn: string,
    deliveryAddressIn: string, weightIn: number, priceIn: number, noteIn: string, responseIn: string,
    statusIn: number, token: string) => axios.post(baseURL, {
      category_id: categoryIdIn,
      customer_id: customerIdIn,
      driver_id: driverIdIn,
      pickup_address: pickupAddressIn,
      delivery_address: deliveryAddressIn,
      weight: weightIn,
      price: priceIn,
      note: noteIn,
      response: responseIn,
      status: statusIn,
  },
  configWithBearerToken(token))

  static update = (idIn: number, categoryIdIn: number, customerIdIn: number, driverIdIn: number, pickupAddressIn: string,
    deliveryAddressIn: string, weightIn: number, priceIn: number, noteIn: string, responseIn: string,
    statusIn: number, token: string) => axios.put(`${baseURL}/${idIn}`, {
      category_id: categoryIdIn,
      customer_id: customerIdIn,
      driver_id: driverIdIn,
      pickup_address: pickupAddressIn,
      delivery_address: deliveryAddressIn,
      weight: weightIn,
      price: priceIn,
      note: noteIn,
      response: responseIn,
      status: statusIn,
  },
  configWithBearerToken(token))

  static delete = (idIn: number, token: string) => axios.delete(`${baseURL}/${idIn}`, configWithBearerToken(token))
}
