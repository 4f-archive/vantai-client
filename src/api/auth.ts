import axios from 'axios'
import { accessToken } from '../stores/store'

const baseURL = 'http://localhost:8000/api/auth'
const configWithBearerToken = (token: string) => ({
  headers: {
    Authorization: `Bearer ${token}`,
  },
})

export default class AuthAPI {
  static login = (emailIn: string, passwordIn: string) => axios.post(`${baseURL}/login`, {
    email: emailIn,
    password: passwordIn,
  })

  static register = (usernameIn: string, naemIn: string,  emailIn: string, passwordIn: string,
    avatarIn: string, phoneIn: string, addressIn: string, roleIn: number, activeIn: number,
    accessToken: string) => axios.post(`${baseURL}/register`, {
    username: usernameIn,
    name: naemIn,
    email: emailIn,
    password: passwordIn,
    password_confirmation: passwordIn,
    avatar: avatarIn,
    phone: phoneIn,
    address: addressIn,
    role: roleIn,
    active: activeIn,
  },
  configWithBearerToken(accessToken))

  static update = (idIn: string, codenameIn: string, nameIn: string, phoneIn: string, emailIn: string, addressIn: string) => axios.put(`${baseURL}/register`, {
    codename: codenameIn,
    name: nameIn,
    phone: phoneIn,
    email: emailIn,
    address: addressIn,
  })

  static me = (token: string) => axios.get(`${baseURL}/me`, configWithBearerToken(token))
}
