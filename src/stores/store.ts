import { writable } from 'svelte/store';

function createAccessToken() {
	const { subscribe, set } = writable("");

	return {
        subscribe,
        set: (value: string) => set(value),
		reset: () => set("")
	};
}

function createUser() {
    const state = {
        name: undefined,
        email: undefined,
    }

	const { subscribe, set, update } = writable(state);

	return {
        subscribe,
        set: (value: any) => {
            update(state => {
                state.name = value.name
                state.email = value.email

                return state
            })
        },
		reset: () => set(state)
	};
}

export const accessToken = createAccessToken();
export const currentUser = createUser();